function filterCart(cart){
  var hasSub = false;
  var hasSample = false;
  for(var i = 0; i < cart.items.length; i++){
    if (cart.items[i].id === 8102646087767) {
      hasSub = true;
      cart.item_count--;
      cart.items[i].hide = true;
    }
    if (cart.items[i].id === 5672401895455) {
      hasSample = true;
    }
  }
  if(hasSub && !hasSample){
    $.post({
      url: "https://skylar.com/cart/update.js",
      data: { updates: { 8102646087767: 0 } }
    });
  }
  return cart;
}
/*
$(document).ready(function() {
  // try to remove the subscriptions item as soon as sample is removed
  $("#updates_5672401895455")
    .siblings(".ss-icon.minus")
    .click(function() {
      removeSubscriptionProduct();
    });

  function removeSubscriptionProduct() {
    console.log("removing sub");
    $("#updates_8102646087767")
      .siblings(".ss-icon.minus")
      .trigger("click");

    // try to remove the item from the api level too
    $.post({
      url: "https://skylar.com/cart/update.js",
      data: { updates: { 8102646087767: 0 } },
      success: function(data) {
        $("input[value='8102646087767']")
          .parents(".cart_item0")
          .remove();
      }
    });
  }

  function removeSoloHiddenSubscription() {
    $.getJSON("/cart.js", function(cart) {
      var hasSample = false;
      var hasSubscription = false;
      for (var i in cart.items) {
        let item = cart.items[i];
        if (String(item.id) === "5672401895455") {
          hasSample = true;
        }
        if (String(item.id) === "8102646087767") {
          hasSubscription = true;
        }
      }
      if (hasSubscription && !hasSample) {
        removeSubscriptionProduct();
      }
    });
  }

  $(".cart-button").click(function() {
    removeSoloHiddenSubscription();
  });

  // check on document ready
  removeSoloHiddenSubscription();
  adjustCartNumberForHiddenProduct();

  // check on document click
  $(document).on("click", function() {
    adjustCartNumberForHiddenProduct();
    $("input[value='8102646087767']")
      .parents(".cart_item0")
      .hide();
  });

  // reduce cart number
  var cartNumSubtracted = false;
  function adjustCartNumberForHiddenProduct() {
    var cartNum = Number($(".cart-count-btn").html());
    console.log("cartNumSubtracted", cartNumSubtracted);
    console.log(
      $("input[value='8102646087767']"),
      $("input[value='8102646087767']").length && !cartNumSubtracted
    );
    if ($("input[value='8102646087767']").length && !cartNumSubtracted) {
      $(".cart-count-btn").html(cartNum - 1);
      cartNumSubtracted = true;
      console.log("subtract by 1");
    }
  }

  $(document).on("change", "#mm-2 > li.mm-label > p", function() {
    cartNumSubtracted = false;
    adjustCartNumberForHiddenProduct();
  });
});
*/