export class contest {
  constructor() {
    this.state = {};
  }

  init() {
    this.modalToggle();
    this.contestVote();
    this.stayInTouch();
  }

  modalToggle() {
    $(".md-overlay").click(() => {
      $(".md-modal").removeClass("md-show");
    });

    $("#modal-close").click(() => {
      $(".md-modal").removeClass("md-show");
    });
  }

  stayInTouch() {
    $("#stay-in-touch-trigger").click(() => {
      $(".md-modal").addClass("md-show");
    });
    const $form = $("form#stay-in-touch");
    $form.submit(e => {
      e.preventDefault();
      // if all required fields are filled out
      $.ajax({
        url: "https://api.formbucket.com/f/buk_ma2Z7cGAWj36WYRtXpvoOwV5",
        method: "POST",
        dataType: "json",
        data: $form.serialize()
      }).done(data => {
        $form.hide();
        $(".form-feedback").show();
      });
    });
  }

  contestVote() {
    const $choice = $(".vote-choices .column-item");
    const $form = $("#contest-vote-form");
    $choice.click(e => {
      var $currentChoice = $(e.currentTarget);
      var $radio = $($currentChoice.data("target"));
      console.log($radio);
      $choice.removeClass("active");
      $currentChoice.addClass("active");
      $radio.prop("checked", true).click();
      console.log($(e.currentTarget));
    });
    $form.submit(e => {
      e.preventDefault();
      if ($form.find("input[type=radio]:checked").length === 0) {
        console.log("not voted");
        $form.find(".error").css({ display: "block" });
        return false;
      }
      $.ajax({
        url: "https://api.formbucket.com/f/buk_Wb4Kze8ybApPpeLbN17NUBWo",
        method: "POST",
        dataType: "json",
        data: $form.serialize()
      })
        .done(data => {
          console.log(data);
          $(".md-modal")
            .css({ top: $(window).scrollTop() + 24 })
            .addClass("md-show");
        })
        .catch(err => {
          console.log(err);
        });
    });
  }
}
