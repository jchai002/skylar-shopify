export class product {
  constructor() {
    this.state = {};
  }

  init() {
    this.waitlist();
  }

  waitlist() {
    $(".md-overlay").click(() => {
      $("#waitlist-modal").removeClass("md-show");
    });
    $(".md-close").click(() => {
      $("#waitlist-modal").removeClass("md-show");
    });
    $("#waitlist-trigger").click(e => {
      $("#waitlist-modal").addClass("md-show");
    });
    const $form = $("form#waitlist-form");
    $form.submit(e => {
      e.preventDefault();
      // if all required fields are filled out
      $.ajax({
        url: "https://api.formbucket.com/f/buk_UTduoDUGPgyexBn03f3EHf2R",
        method: "POST",
        dataType: "json",
        data: $form.serialize()
      }).done(data => {
        $form.hide();
        $(".form-feedback").show();
      });
    });
  }
}
