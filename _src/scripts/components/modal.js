import Cookies from "js-cookie";

export class modal {
  constructor() {
    this.state = {};
  }

  init() {
    const self = this;
    // detect if user is in Europe
    $.get("https://json.geoiplookup.io/api", function(response) {
      if (
        response.continent_code == "EU" &&
        Cookies.get("gdprAgreed") !== "true"
      ) {
        self.openModal();
      }
    });
    $(".agree").click(() => {
      Cookies.set("gdprAgreed", true);
      self.closeModal();
    });
  }

  openModal() {
    $(".modal-overlay").fadeIn();
    $("body").addClass("no-scroll");
  }

  closeModal() {
    $(".modal-overlay").fadeOut();
    $("body").removeClass("no-scroll");
  }
}
