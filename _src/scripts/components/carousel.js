import Cookies from "slick-carousel";

export class carousel {
  constructor() {
    this.state = {};
  }

  init() {
    $(".carousel").slick({
      autoplay: true,
      autoplaySpeed: 6000,
      dots: true,
      arrows: false,
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: "linear"
    });
  }
}
