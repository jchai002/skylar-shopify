import Cookies from "js-cookie";

// utility function to import all files in certain directory
function importAll(r, store) {
  r.keys().forEach(key => {
    var keyString = Object.keys(r(key))[0];
    store[keyString] = r(key)[keyString];
  });
}
// import all components and save in components object
var components = {};
importAll(require.context("./components/", true, /\.js$/), components);

$(document).ready(function() {
  $(function() {
    const pageClass = $(".content-wrapper").data("scope");
    if (components[pageClass]) {
      new components[pageClass]().init();
    }

    // modal script should be run on all pages
    new components["modal"]().init();

    // globally run js

    // check for bold option products, refresh the page to force remove $0 items
    $.getJSON("/cart.js", function(cart) {
      var toRemove = {};
      var hasFree = false;
      for (var i in cart.items) {
        var item = cart.items[i];
        var itemProps = item.properties;
        if (
          itemProps &&
          itemProps["_boldBuilderId"] &&
          String(item.price) === "0"
        ) {
          hasFree = true;
          // set quantity of free item to 0
          toRemove[item.id] = 0;
        }
      }
      if (hasFree) {
        $.post({
          url: "https://skylar.com/cart/update.js",
          data: { updates: toRemove },
          success: function(data) {
            location.reload();
          }
        });
      }
    });

    $("#updates_8328716550231").on("change paste keyup", function() {
      if ($("#updates_8328716550231").val() > 1) {
        console.log("gammin");
      }
    });

    // get larger instagram pics
    $(window).on("load", function() {
      $("img.il-photo__img").each(function() {
        var segs = $(this)
          .attr("src")
          .split("/");
        var filename = segs[segs.length - 1];
        console.log(segs, segs.length, segs[segs.length - 1]);
        var apiUrl =
          "https://scontent.cdninstagram.com/hphotos-xap1/t51.2885-15/s800x800/e35/c0.30.705.705/";
        $(this).attr("src", apiUrl + filename);
      });
    });

    $(document).ready(function() {
      var x = $(".promo_banner").height();
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 0) {
          $(".promo_banner + .mm-fixed-top").css("margin-top", "0px");
        } else {
          $(".promo_banner + .mm-fixed-top").css("margin-top", "60px");
        }
      });
    });

    // GA and pinterest tracking
    pintrk("track", "pagevisit");
    $(".add_to_cart").click(function() {
      ga("send", "event", "addToCart");
      pintrk("track", "addtocart");
    });

    function submitCart() {
      $("form#cart").attr("action", "https://skylar.com/checkout");
      $("form#cart").submit();
    }

    // ajax cart click events
    $("#checkout").click(function(e) {
      if ($(".quantity#updates_8328716550231").val() > 1) {
        e.preventDefault();
        $(".quantity#updates_8328716550231").css({ "border-color": "#ea5557" });
      }
    });
    $(document).on("click", "[name='checkout']", function(e) {
      if ($("#updates_8328716550231").val() > 1) {
        e.preventDefault();
        $("#updates_8328716550231").css({ "border-color": "#ea5557" });
      }

      if (
        $("#cart").find("input[value='8102646087767']").length &&
        !$("#cart").find("input[value='5672401895455']").length
      ) {
        e.preventDefault();
        console.log("only has subscription");
        $.post({
          url: "https://skylar.com/cart/update.js",
          data: { updates: { 8102646087767: 0 } },
          success: function(data) {
            location.reload();
          }
        });
      }

      $.getJSON("/cart.js", function(cart) {
        var hasNonGiftwrap = false;
        for (var i in cart.items) {
          let item = cart.items[i];

          // check to see if is a non-gift wrap
          if (String(item.id) !== "4190508285983") {
            hasNonGiftwrap = true;
          }
        }

        if ($("#add_giftwrap_message").is(":checked") && hasNonGiftwrap) {
          $.post({
            url: "https://skylar.com/cart/add.js",
            data: {
              quantity: 1,
              id: 4190508285983
            },
            success: function(data) {
              submitCart();
            }
          });
        }
      });
    });
    // Philosophy read more button -- Mobile
    var philosophy_row__two =
      ".philosophy-block__two .philosophy-block__item .read__more";
    $(philosophy_row__two).click(function() {
      $(this)
        .parent()
        .find("p")
        .toggleClass("active");
      $(this).toggleClass("active");
    });
  });
});
